﻿# -*- coding: utf-8 -*-
import os, sys
sys.path.append('/home/p/pknovee3/my_boards/todomanager')
sys.path.append('/home/p/pknovee3/.local/lib/python3.6/site-packages')
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
