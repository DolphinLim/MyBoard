from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse

from .models import *
from user.models import *

import uuid

# Представления

@login_required
@csrf_exempt
def todolist_page(request):

    user = Profile.objects.get(user__id = request.user.id)

    context = {
        'title': 'ToDoList',
        'boards': user.board.all()
    }

    return render(request, 'todolist/lists.html', context)

def todolist_anonimous_user(request):

    if (request.method == 'GET'):
        key_board = request.GET.get('key')

        print(key_board)

        board = Board.objects.get(key_share = key_board)

        context = {
            'board': board
        }

        return render(request, 'todolist/view_board.html', context)

@login_required
@csrf_exempt
def todolist_share(request, id):

    if (request.method == 'POST'):
        user = Profile.objects.get(user__id = request.user.id)

        if (user):
            board = Board.objects.get(id = id)
            board.key_share = uuid.uuid4().hex
            board.save()

            return JsonResponse({'id': board.id, 'name': board.name, 'phone': board.phone, 'key': board.key, 'created_at': board.created_at, 'key': board.key_share})
        else:
            return JsonResponse({'error': '205', 'message': 'Ошибка.'})


# Работа с досками
@login_required
@csrf_exempt
def add_board(request):
    if (request.method == 'POST'):

        user = Profile.objects.get(user__id = request.user.id)

        if (user):
            name_board = request.POST['nameBoard']
            phone = request.POST['numberPhone']
            board = Board.objects.create(name = name_board, phone = phone)
            user.board.add(board)

            return JsonResponse({'id': board.id, 'name': board.name, 'phone': board.phone, 'key': board.key, 'created_at': board.created_at})
        else:
            return JsonResponse({'error': '205', 'message': 'Доска не будет создана.'})

@login_required
@csrf_exempt
def update_board(request, id):

    if (request.method == 'POST'):
        user = Profile.objects.all()

        name_board, number_phone = None, None

        try:
            name_board = request.POST['nameBoard']
        except:
            number_phone = request.POST['numberPhone']

        board = Board.objects.get(id = id)

        if (name_board):
            board.name = name_board
        else:
            board.phone = number_phone
        board.save()

        return JsonResponse({'result': board.id, 'redirect': None})

@login_required
@csrf_exempt
def update_board_color(request, id):
    pass

@login_required
@csrf_exempt
def delete_board(request, id):

    if (request.method == 'POST'):
        board = Board.objects.get(id = id)
        board.delete()

        return JsonResponse({'result': 'success', 'redirect': None})


# Работа с задачами
@login_required
@csrf_exempt
def add_task(request):

    if (request.method == 'POST'):
        board_id = request.POST['boardId']
        name_task = request.POST['nameTask']

        task = Task.objects.create(name = name_task)

        if (task):
            board = Board.objects.get(id = board_id)
            board.task.add(task)

        return JsonResponse({'id': task.id, 'name': task.name, 'status': task.status, 'created_at': task.created_at})

@login_required
@csrf_exempt
def update_task(request, id):

    if (request.method == 'POST'):
        name_task = request.POST['nameTask']
        task = Task.objects.get(id = id)
        task.name = name_task
        task.save()

        return JsonResponse({'id': task.id})

@login_required
@csrf_exempt
def success_task(request, id):

    if (request.method == 'POST'):
        task = Task.objects.get(id = id)

        if (task.status):
            task.status = False
        else:
            task.status = True

        task.save()

        return JsonResponse({'status': task.status})

@login_required
@csrf_exempt
def delete_task(request, id):

    if (request.method == 'POST'):
        task = Task.objects.get(id = id)
        task.delete()

        return JsonResponse({'result': 'success', 'redirect': None})
