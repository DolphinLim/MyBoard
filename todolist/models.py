from django.db import models

# Create your models here.
class Board(models.Model):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 32)
    task = models.ManyToManyField('Task')
    key = models.CharField(max_length = 32)
    phone = models.CharField(max_length = 32)
    key_share = models.CharField(max_length = 255)
    color = models.CharField(max_length = 7)
    created_at = models.DateTimeField(auto_now = True)

class Task(models.Model):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 32)
    status = models.BooleanField(default = 0)
    created_at = models.DateTimeField(auto_now = True)
