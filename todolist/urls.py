from .views import *

from django.urls import path

urlpatterns = [
    path('', todolist_page),

    path('add/', add_board),
    path('del/<int:id>/', delete_board),
    path('update/<int:id>/', update_board),
    path('share/<int:id>/', todolist_share),
    path('share/', todolist_anonimous_user),

    path('task/add/', add_task),
    path('task/update/<int:id>/', update_task),
    path('task/del/<int:id>/', delete_task),
    path('task/success/<int:id>/', success_task),
]
