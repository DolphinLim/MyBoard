
/** Отправка форм **/
function sendForm(objForm, action) {
    /** Поля формы **/
    var fields = result = '';

    /** Записываем все поля формы **/
    Array.from(objForm.elements).forEach((field) => {
        fields += `${field.name}=${field.value}&`;
    });

    /** Отправка формы **/
    var postHttp = new XMLHttpRequest();
    postHttp.open('POST', action, false);
    postHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    postHttp.onreadystatechange = function () {
        result = JSON.parse(this.response);

        if (result['redirect']) {
            window.location = result['redirect'];
        }
    };

    postHttp.send(fields);

    /** Возврат результата **/
    return result;

}

/** Отправка POST запросов с параметрами **/
function sendPostQueryParams(action, params) {
    var result = '';
    var postHttp = new XMLHttpRequest();
    postHttp.open('POST', action, false);
    postHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    postHttp.onreadystatechange = function () {
        if (this.status != 200) {
            console.log(this.status);
        } else {
            console.log(this.status);
        }

        result = JSON.parse(this.response);
    };

    postHttp.send(params);

    return result;
}

/** Отправка POST запросов **/
function sendPostQuery(action) {

    var postHttp = new XMLHttpRequest();
    postHttp.open('POST', action, false);
    postHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    postHttp.onreadystatechange = function () {
        if (this.status != 200) {
            console.log(this.status);
        } else {
            console.log(this.status);
        }
    };

    postHttp.send();
}

/** Создание формы **/
function add_board(form, template_tag = null) {
    /** Отправка формы **/
    var result = sendForm(form, '/boards/add/');

    /** Создание доски **/
    var board = document.createElement('div');

    /** Добавление классов к блоку доски **/
    board.className = 'col-lg-4 col-md-4 col-sm-6 col-sm-12 col-xs-12 board';

    /** Создание аттрибутов доски **/
    board.setAttribute('data-phone', result['phone']);                 // Телефон привязанный к доске
    board.setAttribute('data-key',   result['key']);                   // Ключ привязанный к доске
    board.setAttribute('id',         'board-' + result['id']);         // ID для доски
    board.setAttribute('data-task',  0);                               // Кол-во созданных task`ов

    var template = `
        <div class="card-container">
            <div class="flipper">
                <div class="front">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-xs-7 caption-board">${result['name']}</div>
                                    <div class="col-xs-5 text-right"><span class="glyphicon glyphicon-remove" onclick="
                                        delete_board(${result['id']});
                                    "></span></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body text-center">
                            <form method="POST" onsubmit="add_task(this, 'board-' + ${result['id']}); return false;">
                                <div class="message"></div>
                                <div class="col-xs-12 text-left">
                                    <input type="hidden" name="boardId" value="${result['id']}">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Добавить задачу&hellip;" name="nameTask">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-default">+</button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                            <div class="col-xs-12 text-left">
                                <div class="row task-list">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12 text-left task-progress">Выполнено 0/0 задач</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `;

    board.innerHTML = template;

    document.getElementById('boards').appendChild(board);
}

/** Удаление доски **/
function delete_board(id_board) {
    sendPostQuery(`/boards/del/${id_board}/`);
    document.getElementById(`board-${id_board}`).remove();
}


/** Обновление доски имя **/
function update_board_name(params) {
    var result = sendPostQueryParams('/boards/update/' + window.location.hash.split('#')[1] + '/', params)
    var current_board = document.getElementById('board-' + window.location.hash.split('#')[1]);
    //current_board.getElementsByClassName('caption-board')[0].textContent = form.elements.nameBoard.value;
}

/** Обновление доски телефон **/
function update_board_phone(params) {
    var result = sendPostQueryParams('/boards/update/' + window.location.hash.split('#')[1] + '/', params)
    var current_board = document.getElementById('board-' + window.location.hash.split('#')[1]);
    //current_board.getElementsByClassName('caption-board')[0].textContent = form.elements.nameBoard.value;
}

/** Поделиться доской **/
function board_share(id_board) {
    var result = sendPostQueryParams('/boards/share/' + id_board + '/', null);
    alert(window.location.origin.toString() + window.location.pathname.toString() + 'share/?key=' + result['key']);
}

/** Создание задач **/
function add_task(form, id_board) {
    var result = sendForm(form, '/boards/task/add/');

    var day = new Date(result['created_at']).toISOString().split('-')[2].split('T')[0]
    var time_hour = new Date('2018-07-08T13:03:37.405Z').toISOString().split('T')[1].split(':')[0]
    var time_min = new Date('2018-07-08T13:03:37.405Z').toISOString().split('T')[1].split(':')[1]

    var created_at = 'Изм ' + day + ' в ' + time_hour + ':' + time_min;

    /** Создание задачи **/
    var task = document.createElement('div');

    /** Классы применяемые к блоку task **/
    task.className = 'col-xs-12 task';

    /** Атрибуты блока task **/
    task.setAttribute('data-status', result['status']);       // Статус текущего task
    task.setAttribute('id',          `task-${result['id']}`); // ID для task

    /** Шаблон  **/
    var template = `
        <span class="glyphicon glyphicon-remove-sign" onclick="click_task(${result.id}"></span>

        <span style="width: 454px; white-space: normal; padding-right: 10px;" onclick="this.setAttribute('contenteditable', true); location.hash = '${result['id']}'; this.className = 'form-control'; this.style.height = 'auto'; this.style.zIndex = '100';" onblur="update_task('nameTask=' + this.textContent); this.className = '';">
            ${result['name']}
        </span>
        <div class="control-item-task">
            <span class="pub-date" data-toggle="tooltip" data-placement="left" data-original-title="08.07.18">${created_at}</span>
            &nbsp;&nbsp;<span class="glyphicon glyphicon-trash" onclick="
                delete_task(${result['id']});
            "></span>
        </div>
    `;

    /** Добавление шаблона в блок task **/
    task.innerHTML = template;

    /** Добавление task в блок task-list **/
    console.log(id_board);
    var current_board = document.getElementById(id_board);
    var current_task_list = current_board.getElementsByClassName('task-list')[0];
    current_task_list.appendChild(task);
}

/** Удаление задач **/
function delete_task(id_task) {
    sendPostQuery(`/boards/task/del/${id_task}/`);
    document.getElementById(`task-${id_task}`).remove();
}

/** Обновление задачи **/
function update_task(params) {
    var result = sendPostQueryParams('/boards/task/update/' + window.location.hash.split('#')[1] + '/', params);
}

/** Отметка задачи **/
function click_task(id_task) {
    sendPostQuery(`/boards/task/success/${id_task}/`);

    var current_task = document.getElementById('task-' + id_task).getElementsByClassName('glyphicon')[0];

    if (current_task.className == 'glyphicon glyphicon-remove-sign') {
        current_task.className = 'glyphicon glyphicon-ok-sign';
    } else if (current_task.className == 'glyphicon glyphicon-ok-sign') {
        current_task.className = 'glyphicon glyphicon-remove-sign';
    }
}

/** Обновление прогресса **/
function update_progress(id_board) {
    var success_task = 0;
    var current_board = document.getElementById(id_board);
    var progress = current_board.getElementsByClassName('task-progress')[0];

    Array.from(current_board.getElementsByClassName('task-list')[0].children).forEach((task) => {
        if (task.dataset.status == 'true') {
            success_task += 1;
        }
    });

    progress.textContent = 'Выполнено ' + success_task + '/' + current_board.getElementsByClassName('task-list')[0].children.length + ' задач';
}

/** Загрузка прогрессов **/
function load_progress() {
    var boards = document.getElementById('boards');
    Array.from(boards.children).forEach((board) => {
        update_progress(board.id);
    });
}
