from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.http import JsonResponse

from django.shortcuts import render, redirect

# Create your views here.
def login(request):

    if (request.user.is_authenticated):
        return redirect('/boards/')

    if (request.method == 'POST'):
        username = request.POST['username']
        passwd = request.POST['passwd']

        user = authenticate(username = username, password = passwd)

        if user is not None:
            auth_login(request, user)
            return JsonResponse({'result': 'success', 'redirect': '/'})
        else:
            return JsonResponse({'result': None, 'redirect': None})

    context = {
        'page': 'Вход',
    }

    return render(request, 'user/login.html', context)

def register(request):

    if (request.user.is_authenticated):
        return redirect('/boards/')

    context = {
        'page': 'Регистрация',
    }

    return render(request, 'user/register.html', context)

def logout(request):

    auth_logout(request)

    return redirect('/')
