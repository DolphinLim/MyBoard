from django.db import models
from todolist.models import Board
from django.contrib.auth.models import User

# Create your models here.
class Profile(models.Model):
    id = models.AutoField(primary_key = True)
    user = models.OneToOneField(User, on_delete = models.CASCADE)
    board = models.ManyToManyField(Board)
    created_at = models.DateTimeField(auto_now = True)
