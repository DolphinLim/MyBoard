from .views import *

from django.urls import path

urlpatterns = [
    path('', login),
    path('register/', register),
    path('logout/', logout),
]
